import csv
import random
# generation coord
coord = [[random.randrange(20) for i in range(2)] for j in range(10)]
print(coord)
# export coord in txt&csv
with open("output.csv", "w", newline="") as file:
    writer = csv.writer(file)
    writer.writerows(coord)
with open("output.txt", "w") as file:
    for row in coord:
        file.write(str(row[0]) + " " + str(row[1]) + "\n")

